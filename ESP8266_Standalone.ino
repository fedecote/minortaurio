//#define ENA   14          // Enable/speed motors Right        GPIO14(D5)
//#define ENB   12          // Enable/speed motors Left         GPIO12(D6)
#define IN_1  15          // L298N in1 motors Right           GPIO15(D8)
#define IN_2  13          // L298N in2 motors Right           GPIO13(D7)
#define IN_3  2           // L298N in3 motors Left            GPIO2(D4)
#define IN_4  0           // L298N in4 motors Left            GPIO0(D3)
#define in1Rodillo  16
#define in2Rodillo  5
#define in3Rodillo  14
#define in4Rodillo  12

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <PubSubClient.h>

String command;             //String to store app command state.
int speedCar = 1000;         // 400 - 1023.
int speed_Coeff = 3;

const char* ssid = "theonlipa";
const char* password = "lipacasa";
const char* mqtt_server = "192.168.0.121";
bool isAttacking = false;
char movement;
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
 Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT); 
  pinMode(in1Rodillo, OUTPUT);
  pinMode(in2Rodillo, OUTPUT);
  pinMode(in3Rodillo, OUTPUT);
  pinMode(in4Rodillo, OUTPUT);    
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  switch ((char)payload[0]) {
    case '0':
      isAttacking = false;
      break;
    case '1':
      isAttacking = true;  
      break;
    case '2':
      movement = (char)payload[0];
      break;
    case '3':
      movement = (char)payload[0];
      break;
    case '4':
      movement = (char)payload[0];
      break;
    case '5':
      movement = (char)payload[0];
      break;
    default:
      movement = '6';
      break;
}}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("pepe","root","root")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.subscribe("minORTauro");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(2000);
    }
  }
}

void attack(){
  digitalWrite(in1Rodillo, HIGH);
  digitalWrite(in2Rodillo, LOW);
  digitalWrite(in4Rodillo, HIGH);
  digitalWrite(in3Rodillo, LOW);
}

void stopAttack(){  
      digitalWrite(in1Rodillo, LOW);
      digitalWrite(in2Rodillo, LOW);
      digitalWrite(in3Rodillo, LOW);
      digitalWrite(in4Rodillo, LOW);
}

void goAhead(){ 
      digitalWrite(IN_1, HIGH);
      digitalWrite(IN_2, LOW);
      digitalWrite(IN_3, LOW);
      digitalWrite(IN_4, HIGH);
  }

void goLeft(){ 
      digitalWrite(IN_1, HIGH);
      digitalWrite(IN_2, LOW);
      digitalWrite(IN_3, LOW);
      digitalWrite(IN_4, LOW);
  }

void goRight(){ 
      digitalWrite(IN_1, LOW);
      digitalWrite(IN_2, LOW);
      digitalWrite(IN_3, LOW);
      digitalWrite(IN_4, HIGH);
}

void goBack(){
      digitalWrite(IN_1, LOW);
      digitalWrite(IN_2, HIGH);
      digitalWrite(IN_3, HIGH);
      digitalWrite(IN_4, LOW);
}

void stopCar(){  

      digitalWrite(IN_1, LOW);
      digitalWrite(IN_2, LOW);
      //analogWrite(ENA, speedCar);

      digitalWrite(IN_3, LOW);
      digitalWrite(IN_4, LOW);
      //analogWrite(ENB, speedCar);
 }

void loop() {
  if (!client.connected()) {
    movement = '6';
    isAttacking = '0';
    reconnect();
  }
  client.loop();
  switch (movement) {
    case '2':
      goAhead();
      break;
    case '3':
      goBack();
      break;
    case '4':
      goRight();
      break;
    case '5':
      goLeft();
      break;
    default:
      stopCar();
      break;
  } 
  switch (isAttacking) {
    case true:
      attack();
      break;
    default:
      stopAttack();
      break;
  }
  }
