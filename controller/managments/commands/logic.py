from django.core.management.base import BaseCommand, CommandError
import time
from controller.models import Dance, Movement
from django.core.exceptions import ObjectDoesNotExist
import paho.mqtt.client as mqtt
import json


class Command(BaseCommand):
    # Define event callbacks
    def on_connect(client, userdata, flags, rc):
        print("rc: " + str(rc))

    def on_message(client, obj, msg):
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    def on_publish(client, obj, mid):
        print("mid: " + str(mid))

    def on_subscribe(client, obj, mid, granted_qos):
        print("Subscribed: " + str(mid) + " " + str(granted_qos))

    def on_log(client, obj, level, string):
        print(string)

    mqttc = mqtt.Client()
    # Assign event callbacks
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish
    mqttc.on_subscribe = on_subscribe

    # Connect
    mqttc.username_pw_set("root", "root")
    mqttc.connect("127.0.0.1", port=1883)

    # Start subscribe, with QoS level 0
    mqttc.subscribe("minORTauro", 0)

    # Continue the network loop, exit when an error occurs
    #rc = 0
    #while rc == 0:
    #    rc = mqttc.loop()
    #print("rc: " + str(rc))
        
    def on_connect(self, client, obj, flags, rc):
        print("Conectado con codigo: "+str(rc))
        client.subscribe("minORTauro")
        
    def on_message(self, msg):
        print (msg.topic+" -> "+ str(msg.payload))
        m_in=json.loads(msg.payload)
        if(m_in["Action"] == "Dance"):
            print("Dancing")
            self.dance(m_in)
        if(m_in["Action"] == "0" or m_in["Action"] == "1" ):
            self.attack(m_in)
        elif (m_in["Action"] != "Attack" and m_in["Action"] > 1):
            print("Move")
            self.move(m_in)
    
    def handle(self, *args, **options):
        client.loop_start()
        while True:
            pass
        
    def publishMessage(self, message):
        print(message["Action"])
        self.mqttc.publish("minORTauro", message["Action"])
    
    def move(self, message):
        response = {"Action": message["Action"]}
        self.publishMessage(self, response)
        print(Movement.objects.get(value = int(message["Action"])).name)
    
    def dance(self, message):
        dance = Dance.objects.get(name = message["Name"])         
        for movement in dance.movements.all():
            response = {"Action": movement.name}
            self.publishMessage(self, response)
            time.sleep(5)
            
    def attack(self, message):
        response = {"Action": message["Action"]}
        self.publishMessage(self, response)
        if(message["Action"] == "0"):
            print("Descansar")
        elif(message["Action"] == "1"):
            print("Atacar")