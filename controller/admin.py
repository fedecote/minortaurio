from django.contrib import admin
from .models import Dance, Movement, DanceMovement

# Register your models here.
admin.site.register(Dance)
admin.site.register(Movement)
admin.site.register(DanceMovement)
