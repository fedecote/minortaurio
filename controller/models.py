from django.db import models

class Movement(models.Model):
    name = models.CharField(max_length=20, unique=True)
    value = models.IntegerField()
    def __str__(self):
        return self.name + " " + str(self.value)
	
class Dance(models.Model):
	name = models.CharField(max_length=200, unique=True)
	movements = models.ManyToManyField(Movement, through='DanceMovement')
	def get_movements(self):
		return ", ".join([d.name for d in self.movements.all()])
	def __str__(self):              # __unicode__ on Python 2
		return self.name + ', Movements:' + self.get_movements()

class DanceMovement(models.Model):
    dance = models.ForeignKey(Dance, on_delete=models.CASCADE,)
    movement = models.ForeignKey(Movement, on_delete=models.CASCADE,)
    def __str__(self):
        return self.dance.name + " " + self.movement.name