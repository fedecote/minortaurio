from django.http import HttpResponse
from django.template import loader
from .models import Movement, Dance, DanceMovement
from .managments.commands import logic


def index(request):
    template = loader.get_template('controller/home.html')
    context = {
    }
    if request.method == 'POST' and 'forward' in request.POST:
        logic.Command.move(logic.Command, {"Action":str(Movement.objects.get(name="Adelante").value)}) 
    if request.method == 'POST' and 'back' in request.POST:
        logic.Command.move(logic.Command, {"Action":str(Movement.objects.get(name="Atras").value)}) 
    if request.method == 'POST' and 'right' in request.POST:
        logic.Command.move(logic.Command, {"Action":str(Movement.objects.get(name="Derecha").value)}) 
    if request.method == 'POST' and 'left' in request.POST:
        logic.Command.move(logic.Command, {"Action":str(Movement.objects.get(name="Izquierda").value)})   
    if request.method == 'POST' and 'stop' in request.POST:
        logic.Command.move(logic.Command, {"Action":str(Movement.objects.get(name="Parar").value)}) 
    if request.method == 'POST' and 'attack' in request.POST:
        logic.Command.attack(logic.Command, {"Action":str(Movement.objects.get(name="Atacar").value)}) 
    if request.method == 'POST' and 'rest' in request.POST:
        logic.Command.attack(logic.Command, {"Action":str(Movement.objects.get(name="Descansar").value)}) 
    # return user to required page
    return HttpResponse(template.render(context, request))

def dances(request):  
    dances_list = Dance.objects.all()
    template = loader.get_template('controller/dances.html')
    context = {
        'dances_list': dances_list,
    }
    if request.method == 'POST':
        for dance in Dance.objects.all():
            if(dance.name in request.POST):
                logic.Command.dance(logic.Command, {"Name":dance.name})   
    return HttpResponse(template.render(context, request))

def editDance(request, name):
    dance = Dance.objects.get(name=name)
    movements_list = Movement.objects.all()
    template = loader.get_template('controller/newDance.html')
    context = {
            'dance': dance,
            'movements_list': movements_list
        }
    return HttpResponse(template.render(context, request))

def dance(request):
    dance_name=request.POST.get("name")
    movements_list = Movement.objects.all()
    context = {
        'movements_list': movements_list
    }
    template = loader.get_template('controller/newDance.html')
    if(dance_name):
        if(Dance.objects.filter(name = dance_name).count() > 0):
            dance = Dance.objects.get(name=dance_name)
        else:
            dance = Dance(name = dance_name)
            dance.save()
        for movement in Movement.objects.all():
            if(movement.name in request.POST):
                danceMove = DanceMovement(dance=dance, movement=movement)
                danceMove.save()
                dance.movements.add(movement)  
                dance.save() 
        context = {
            'dance': dance,
            'movements_list': movements_list
        }
    return HttpResponse(template.render(context, request))
        
    