from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('dances/', views.dances, name = 'dances'),
    path('dance', views.dance, name = 'dance'),
    path('dance/<str:name>', views.editDance, name = 'dance'),
]