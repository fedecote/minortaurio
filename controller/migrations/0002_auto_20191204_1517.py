# Generated by Django 2.2.7 on 2019-12-04 18:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dance',
            name='id',
        ),
        migrations.RemoveField(
            model_name='movement',
            name='id',
        ),
        migrations.AlterField(
            model_name='dance',
            name='name',
            field=models.CharField(max_length=200, primary_key=True, serialize=False, unique=True),
        ),
        migrations.AlterField(
            model_name='movement',
            name='name',
            field=models.CharField(max_length=20, primary_key=True, serialize=False, unique=True),
        ),
    ]
